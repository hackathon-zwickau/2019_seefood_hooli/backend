# backend

All files that have been used to build the backend including files for training, api, item-generator for mongodb and recipes as json.

## Setup

Requirements
* git
* Maven
    * with OpenJDK11
* Docker
* Docker Compose

Steps
1. git clone
2. cd backend/
3. docker-compose up -d
4. mvn clean install
5. cd target/
6. java -jar seefood.backend-0.0.1-SNAPSHOT.jar