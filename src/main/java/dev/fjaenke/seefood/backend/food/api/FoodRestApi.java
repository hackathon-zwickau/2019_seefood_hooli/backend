package dev.fjaenke.seefood.backend.food.api;

import dev.fjaenke.seefood.backend.food.Food;
import dev.fjaenke.seefood.backend.food.persistence.FoodRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/food")
public class FoodRestApi {

        @Autowired
        FoodRepository foodRepository;

        public FoodRestApi(FoodRepository foodRepository){
            this.foodRepository = foodRepository;
        }

        @GetMapping("/withId/{id}")
        public ResponseEntity<Food> getFoodById(@PathVariable String id){
            Optional<Food> foodOptional = foodRepository.findById(id);
            if(foodOptional.isEmpty())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(foodOptional.get(), HttpStatus.OK);
        }

        @GetMapping("/withTitle/{title}")
        public ResponseEntity<Food> getFoodByTitle(@PathVariable String title){
            Food food = foodRepository.findTopByTitle(title);
            return new ResponseEntity<>(food, HttpStatus.OK);
        }

        @GetMapping
        public ResponseEntity<List<Food>> getFoods(){
            List<Food> foods = foodRepository.findAll();
            return new ResponseEntity<>(foods, HttpStatus.OK);
        }

        /**
         * Rest endpoint for creating a recipe
         * @param file
         * @return
         */
        @PostMapping("/uploadRecipe")
        public ResponseEntity<Food> uploadRecipe(@RequestParam("file") MultipartFile file){
            try {
                String content = new String(file.getBytes());
                Food food = Food.createFromJson(content);
                foodRepository.save(food);
                return new ResponseEntity<>(food,HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        /**
         * Rest endpoint for creating multiple recipes
         * @param file
         * @return
         */
        @PostMapping("/uploadRecipes")
        public ResponseEntity<String> uploadRecipes(@RequestParam("file") MultipartFile file){
            try {
                String content = new String(file.getBytes());
                JSONObject j = new JSONObject(content);
                JSONArray a = j.getJSONArray("content");
                for(var o : a){
                    Food food = Food.createFromJson(o.toString());
                    foodRepository.save(food);
                }
                return new ResponseEntity<>("food",HttpStatus.CREATED);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        @PostMapping("/food")
        public ResponseEntity<Food> debug(@RequestParam("food") String food){
           System.out.println(food);
           return new ResponseEntity<>(HttpStatus.OK);
        }



}
