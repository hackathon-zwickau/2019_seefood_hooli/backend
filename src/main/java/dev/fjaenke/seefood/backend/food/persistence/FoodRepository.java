package dev.fjaenke.seefood.backend.food.persistence;

import dev.fjaenke.seefood.backend.food.Food;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FoodRepository extends MongoRepository<Food,String> {
    public Food findTopByTitle(String title);
}
