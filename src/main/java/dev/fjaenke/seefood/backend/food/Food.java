package dev.fjaenke.seefood.backend.food;

import org.json.JSONObject;
import org.springframework.data.annotation.Id;

public class Food {

    @Id
    private String title;
    private String description;

    public Food(String title,String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public static Food createFromJson(String jsonFile){
        JSONObject obj = new JSONObject(jsonFile);
        String title = obj.get("title").toString();
        String description = obj.get("description").toString();
        return new Food(title, description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Food{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
