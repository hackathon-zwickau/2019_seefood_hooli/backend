package dev.fjaenke.seefood.backend.prediction;

import dev.fjaenke.seefood.backend.prediction.model.PredictionResult;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/upload")
public class PredictionApi {

    private final PredictionController predictionController;

    @Autowired
    public PredictionApi(PredictionController predictionController) {
        this.predictionController = predictionController;
    }

    /**
     * Endpoint for uploading image  as Base64 String
     * Stores the images in the ./upload folder
     * Returns a prediction result with the highest prediction
     *
     * @param
     * @return
     */
    @PostMapping(value = "/string", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PredictionResult> upload(@RequestParam("image") String imageB64String) {
        return isValidString(imageB64String) ?
                new ResponseEntity<>(predictionController.upload(imageB64String), HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Endpoint for uploading image as file
     * Stores the image in the ./upload folder
     * Returns a prediction result with the highest prediction
     *
     * @param
     * @return
     */
    @PostMapping(value = "/file", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PredictionResult> upload(@RequestParam("image") MultipartFile imageFile) {
        return isValidFile(imageFile) ?
                new ResponseEntity<>(predictionController.upload(imageFile), HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @PostMapping("/move")
    public ResponseEntity<HttpStatus> move(@RequestParam("predictionId") String predictionId, @RequestParam("expected") String title) {
        try {
            predictionController.move(predictionId, title);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    public ResponseEntity<List<PredictionResult>> getAllUploads(){
        return new ResponseEntity<>(predictionController.getAll(),HttpStatus.OK);
    }

    private boolean isValidString(String imageB64String) {
        return Base64.isBase64(imageB64String);
    }

    private boolean isValidFile(MultipartFile imageFile) {
        String contentType = imageFile.getContentType();
        if(Objects.equals(contentType, "image/png")) return true;
        if(Objects.equals(contentType, "image/jpg")) return true;
        return Objects.equals(contentType, "image/jpeg");
    }

}