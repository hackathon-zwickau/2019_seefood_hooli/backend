package dev.fjaenke.seefood.backend.prediction;

import dev.fjaenke.seefood.backend.prediction.model.PredictionResult;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
public class PredictionController {

    private final Path uploadLocation = Paths.get("./upload");
    private final Path predictedLocation = Paths.get("./predicted");
    private final PredictionRepository predictionRepository;

    public PredictionController(PredictionRepository predictionRepository) {
        this.predictionRepository = predictionRepository;
    }

    public PredictionResult upload(String b64encodedString) {
        try {
            return storeThenPredictImage(Base64.getMimeDecoder().decode(b64encodedString));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return PredictionResult.empty();
    }

    public PredictionResult upload(MultipartFile imageFile) {
        try {
            return storeThenPredictImage(imageFile.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return PredictionResult.empty();
    }

    public void move(String predictionId, String title) throws IOException {
        Optional<PredictionResult> optPredictionResult = predictionRepository.findById(predictionId);
        if(optPredictionResult.isPresent()){
            PredictionResult predictionResult = optPredictionResult.get();
            File destination = new File(predictedLocation + "/" + title + "/" + predictionId + ".jpg");
            if (!destination.exists()) destination.mkdirs();
            Files.move(Paths.get(predictionResult.getLocation()), Paths.get(destination.toURI()), StandardCopyOption.REPLACE_EXISTING);
            PredictionResult updatedResult = PredictionResult.withUpdatedLocation(predictionResult, destination.getPath());
            predictionRepository.save(updatedResult);
        }
    }

    public List<PredictionResult> getAll(){
        return predictionRepository.findAll();
    }

    private PredictionResult storeThenPredictImage(byte[] imageBytes) throws IOException {
        File image = new File(uploadLocation + "/" + UUID.randomUUID() + ".jpg");
        FileUtils.writeByteArrayToFile(image, imageBytes);
        return predictionRepository.save(predict(image));
    }

    private PredictionResult predict(File uploadedFile) throws IOException {
        PostMethod postMethod = postData(uploadedFile);
        String predictionId = postMethod.getResponseHeader("predictionId").getValue();
        String result = postMethod.getResponseBodyAsString();
        return PredictionResult.fromJSON(predictionId, new JSONObject(result), uploadedFile.getPath());
    }

    private PostMethod postData(File file) {
        String result = "No Result";
        PostMethod postMethod = new PostMethod("http://localhost:8000/predict");
        try {
            Part[] parts = {new FilePart("file", file)};
            postMethod.setRequestEntity(new MultipartRequestEntity(parts, postMethod.getParams()));
            HttpClient client = new HttpClient();
            client.executeMethod(postMethod);
            return postMethod;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postMethod;
    }


}
