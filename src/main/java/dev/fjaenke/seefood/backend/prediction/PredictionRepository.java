package dev.fjaenke.seefood.backend.prediction;

import dev.fjaenke.seefood.backend.prediction.model.PredictionResult;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PredictionRepository extends MongoRepository<PredictionResult,String> {
    @Override
    Optional<PredictionResult> findById(String s);
}
