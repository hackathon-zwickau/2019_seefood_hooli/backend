package dev.fjaenke.seefood.backend.prediction.model;

public class Prediction implements Comparable<Prediction>{

    private final String food;
    private final float probability;

    private Prediction(String food, float probability){
        this.food = food;
        this.probability=probability;
    }

    public static Prediction createPrediction(String predictedFood, float probability){
        return new Prediction(predictedFood, probability);
    }

    public String getFood() {
        return food;
    }

    public float getProbability() {
        return probability;
    }

    /**
     * Compares this Prediction with other Prediction
     * Returns the Prediction with the higher probability value
     * @param other
     * @return
     */
    @Override
    public int compareTo(Prediction other) {
        return Float.compare(probability, other.probability);
    }

    @Override
    public String toString() {
        return "Prediction{" +
                "food='" + food + '\'' +
                ", probability=" + probability +
                '}';
    }
}
