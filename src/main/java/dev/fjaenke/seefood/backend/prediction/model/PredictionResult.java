package dev.fjaenke.seefood.backend.prediction.model;

import org.json.JSONObject;
import org.springframework.data.annotation.Id;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class PredictionResult {

    @Id
    private final String predictionId;
    private final Prediction predicted;
    private final List<Prediction> predictions;
    private final String location;

    private PredictionResult(String predictionId, Prediction predicted, List<Prediction> predictions, String location) {
        this.predictionId = predictionId;
        this.predicted = predicted;
        this.predictions = predictions;
        this.location = location;
    }

    public static PredictionResult fromJSON(String predictionId, JSONObject obj, String location) {
        List<Prediction> predictions = new ArrayList<>();
        for (String key : obj.keySet()) {
            predictions.add(Prediction.createPrediction(key, obj.getFloat(key)));
        }
        Collections.sort(predictions, Collections.reverseOrder());
        List<Prediction> _predictions = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            _predictions.add(predictions.get(i));
        }
        Prediction predicted = getBestResult(predictions);
        return new PredictionResult(predictionId, predicted, _predictions, location);
    }

    private static Prediction getBestResult(List<Prediction> predictions) {
        return Collections.max(predictions);
    }

    public static PredictionResult empty() {
        return new PredictionResult(UUID.randomUUID().toString(), Prediction.createPrediction("error", 100f), new ArrayList<>(), "");
    }

    public static PredictionResult withUpdatedLocation(PredictionResult predictionResult, String location){
        return new PredictionResult(predictionResult.getPredictionId(), predictionResult.getPredicted(), predictionResult.getPredictions(), location);
    }

    public String getPredictionId() {
        return predictionId;
    }

    public Prediction getPredicted() {
        return predicted;
    }

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "PredictionResult{" +
                "predictionId='" + predictionId + '\'' +
                ", predicted=" + predicted +
                ", predictions=" + predictions +
                '}';
    }
}
